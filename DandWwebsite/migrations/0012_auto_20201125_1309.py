# Generated by Django 3.1.3 on 2020-11-25 07:39

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DandWwebsite', '0011_auto_20201125_1305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogdata',
            name='description',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='blogdata',
            name='part1',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='blogdata',
            name='part2',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='blogdata',
            name='part3',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='blogdata',
            name='title',
            field=models.CharField(blank=True, max_length=300, null=True),
        ),
    ]
