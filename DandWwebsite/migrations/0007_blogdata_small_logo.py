# Generated by Django 3.1.3 on 2020-11-25 07:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DandWwebsite', '0006_blogdata'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogdata',
            name='small_logo',
            field=models.ImageField(blank=True, default='images/casestudy.jpg', null=True, upload_to='images'),
        ),
    ]
