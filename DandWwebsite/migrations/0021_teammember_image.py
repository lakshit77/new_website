# Generated by Django 3.1.3 on 2020-11-27 13:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DandWwebsite', '0020_teammember'),
    ]

    operations = [
        migrations.AddField(
            model_name='teammember',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='.'),
        ),
    ]
