# Generated by Django 3.1.3 on 2020-11-27 07:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DandWwebsite', '0018_auto_20201125_1400'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogdata',
            name='list_image',
            field=models.ImageField(blank=True, null=True, upload_to='.'),
        ),
    ]
