# Generated by Django 3.1.3 on 2020-11-25 08:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DandWwebsite', '0015_auto_20201125_1330'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogdata',
            name='small_logo',
            field=models.ImageField(blank=True, null=True, upload_to='images'),
        ),
    ]
