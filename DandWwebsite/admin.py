from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(companyInfo)
admin.site.register(BlogData)
admin.site.register(teamMember)
admin.site.register(caseStudy)


