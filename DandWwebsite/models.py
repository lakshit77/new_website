from django.db import models
from ckeditor.fields import RichTextField
# Create your models here.

class companyInfo(models.Model):
    email_id = models.EmailField(null=True, blank=True)
    about_us = RichTextField(null=True, blank=True)
    vision = RichTextField(null=True, blank=True)
    mission = RichTextField(null=True, blank=True)
    about_us = RichTextField(null=True, blank=True)
    number_1 = models.CharField(max_length=100, null=True, blank=True)
    number_2 = models.CharField(max_length=100, null=True, blank=True)
    address = models.CharField(max_length=500, null=True, blank=True)
    linkdein_link = models.CharField(max_length=500, null=True, blank=True)
    twitter_link = models.CharField(max_length=500, null=True, blank=True)
    map_location = models.CharField(max_length=1000, null=True, blank=True)

    def __str__(self):
        return self.email_id


class BlogData(models.Model):
    belong_choices = (  
        ('Expertise','Expertise'),
        ('IndustryServe','IndustryServe'),
        ('CaseStudy','CaseStudy'),
    )
    title = models.CharField(null=True, blank=True, max_length=300)
    subtitle = models.CharField(null=True, blank=True, max_length=500)
    description = models.CharField(null=True, blank=True, max_length=1000)
    part1 = RichTextField(null=True, blank=True)
    part2 = RichTextField(null=True, blank=True)
    part3 = RichTextField(null=True, blank=True)
    small_logo = models.ImageField(upload_to='expertise', blank=True, null=True) 
    list_image = models.ImageField(upload_to='expertise', blank=True, null=True) 
    image1 = models.ImageField(upload_to='expertise', blank=True, null=True) 
    image2 = models.ImageField(upload_to='expertise', blank=True, null=True) 
    image3 = models.ImageField(upload_to='expertise', blank=True, null=True) 
    category = models.CharField(null=True, blank=True, max_length=20)
    belong_to = models.CharField(choices = belong_choices, null=True, max_length=50, default='CaseStudy', blank=True)
    conclusion = RichTextField(null=True, blank=True)

    def __str__(self):
        return self.title

class caseStudy(models.Model):
    title = models.CharField(null=True, blank=True, max_length=100)
    subtitle = models.CharField(null=True, blank=True, max_length=500)
    description = RichTextField(null=True, blank=True)
    analysis = RichTextField(null=True, blank=True)
    recommend = RichTextField(null=True, blank=True)
    pros_cons = RichTextField(null=True, blank=True)
    conclusion = RichTextField(null=True, blank=True)
    category = models.CharField(null=True, blank=True, max_length=100)
    image = models.ImageField(default='img/casestudy.jpg', upload_to='img')

    def __str__(self):
        return self.title


class teamMember(models.Model):
    firstname = models.CharField(null=True, blank=True, max_length=300)
    lastname = models.CharField(null=True, blank=True, max_length=300)
    position = models.CharField(null=True, blank=True, max_length=300)
    image = models.ImageField(upload_to='.', blank=True, null=True) 
    rank = models.CharField(null=True, blank=True, max_length=300)
    content = RichTextField(null=True, blank=True)
    linkdein_link = models.CharField(max_length=500, null=True, blank=True)
    twitter_link = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.firstname
    
    
    

