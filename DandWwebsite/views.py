from django.shortcuts import render
from .models import *

from django.template.defaulttags import register

@register.filter
def even_odd(value):
    if value % 2 == 0:
        return 2
    else:
        return 1

@register.filter
def left_right(value):
    if value % 2 == 0:
        return 'right'
    else:
        return 'left'

        
# Create your views here.

def home(request):
    data = companyInfo.objects.all().first()
    expertise = BlogData.objects.filter(belong_to = 'Expertise')
    context = {
        'data':data, 
        'expertise':expertise
    }
    return render(request, 'DandWwebsite/main.html', context)

def about_us(request):
    data = companyInfo.objects.all().first()
    team_info = teamMember.objects.all().order_by('rank')
    context = {
        'data':data,
        'team_info':team_info
    }
    return render(request, 'DandWwebsite/about_us.html', context)

def CasePage(request):
    data = companyInfo.objects.all().first()
    all_case = caseStudy.objects.all()
    context = {
        'data':data,
        'all_case':all_case
    }
    return render(request, 'DandWwebsite/allBlog.html', context)

def single_case_study(request, pk):
    data = companyInfo.objects.all().first()
    single_case = caseStudy.objects.get(id = pk)
    context = {
        'single_case' : single_case,
        'data':data
    }

    return render(request, 'DandWwebsite/case_study_blog.html', context)

def single_blog(request, pk):
    data = companyInfo.objects.all().first()
    single_case = BlogData.objects.get(id = pk)
    context = {
        'single_case' : single_case,
        'data':data
    }

    return render(request, 'DandWwebsite/case_study_home.html', context)
