from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('about_us/', about_us, name='about_us'),
    path('case/', CasePage, name='Case'),
    path('case/<int:pk>', single_case_study, name='single_case_study'),
    # path('blog/', BlogPage, name='Blog'),
    path('blog/<int:pk>', single_blog, name='single_blog'),
]